# frozen_string_literal: true

class BubbleSort
  def self.sort(list = [])
    do_it_again = false
    sorted_list = list.compact.each_slice(2).flat_map do |this_value, next_value|
      if next_value.to_i < this_value
        do_it_again = true
        [next_value, this_value]
      else
        [this_value, next_value]
      end
    end
    sorted_list = sort(sorted_list) if do_it_again
    sorted_list
  end
end

# list.each_slice(2).flat_map do |next_value, this_value|
#   case next_value <=> this_value
#   when -1
#     []
#   when 1
#   else
#   end
# end