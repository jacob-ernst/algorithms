require 'minitest/autorun'
require 'minitest/pride'
require_relative 'bubble_sort'

class BubbleSortTest < Minitest::Unit::TestCase
  def setup
    @sorted_numbers = (1..10).to_a
  end

  def teardown
    # Do nothing
  end

  def test_unsorted_list
    shuffled_list = @sorted_numbers.shuffle
    assert BubbleSort.sort(shuffled_list), @sorted_numbers
  end

  def test_sorted_list
    assert BubbleSort.sort(@sorted_numbers), @sorted_numbers
  end

  def test_odd_number_of_items
    sorted_list = (20..25).to_a
    assert BubbleSort.sort(sorted_list.shuffle), sorted_list
  end
end
