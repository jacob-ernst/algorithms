package bubble_sort

func Sort(numbers []int) []int  {
	doItAgain := false
	limit := len(numbers) - 1

	for i, val := range numbers{
		if i == limit {
			break
		}

		nextVal := numbers[i + 1]

		if nextVal < val {
			numbers[i] = nextVal
			numbers[i + 1] = val
			doItAgain = true
		}
	}

	if doItAgain {
		Sort(numbers)
	}

	return numbers
}
