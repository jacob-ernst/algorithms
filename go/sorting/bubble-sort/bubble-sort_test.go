package bubble_sort

import (
	"testing"
)

var tests = []struct {
	name 	string
	input    []int
	expected []int
}{
	{
		name: "Sorted Slice",
		input: []int{1, 2, 3},
		expected: []int{1, 2, 3},
	},
	{
		name: "Unsorted Slice",
		input: []int{3, 1, 2},
		expected: []int{1, 2, 3},
	},
}

func TestSort(t *testing.T) {
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ans := Sort(tt.input)
			for i, v := range ans {
				if v != tt.expected[i] {
					t.Errorf("got %d, want %d", ans, tt.expected[i])
				}
			}
		})
	}
}
